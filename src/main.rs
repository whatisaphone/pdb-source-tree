#![warn(future_incompatible, rust_2018_compatibility, rust_2018_idioms, unused)]
#![warn(clippy::pedantic)]
#![allow(clippy::single_match)]
#![cfg_attr(feature = "strict", deny(warnings))]

use pdb::FallibleIterator;
use std::{collections::HashMap, error::Error, fs, path::PathBuf};
use structopt::StructOpt;

fn main() {
    Command::from_args().run().unwrap();
}

#[derive(StructOpt)]
struct Command {
    path: PathBuf,
}

impl Command {
    pub fn run(self) -> Result<(), Box<dyn Error>> {
        let file = fs::File::open(&self.path)?;
        let mut pdb = pdb::PDB::open(file)?;
        let files = read(&mut pdb)?;
        emit(&files);
        Ok(())
    }
}

fn read<'s>(
    pdb: &mut pdb::PDB<'s, impl pdb::Source<'s> + 's>,
) -> pdb::Result<Vec<SourceFile>> {
    let mut files = HashMap::new();

    let debug_info = pdb.debug_information()?;
    let mut modules = debug_info.modules()?;
    while let Some(module) = modules.next()? {
        let module_info = match pdb.module_info(&module)? {
            Some(info) => info,
            None => continue,
        };

        let line_program = module_info.line_program()?;

        let mut symbols = module_info.symbols()?;
        while let Some(symbol) = symbols.next()? {
            match symbol.parse() {
                Ok(pdb::SymbolData::Procedure(data)) => {
                    let mut lines = line_program.lines_at_offset(data.offset);
                    // Assume the first line_info is also the first line in the source.
                    let line_info = match lines.next()? {
                        Some(info) => info,
                        None => continue,
                    };

                    let file_info = line_program.get_file_info(line_info.file_index)?;

                    let out_file_symbols =
                        files.entry(file_info.name).or_insert_with(Vec::new);
                    out_file_symbols.push(SourceSymbol {
                        name: symbol.name()?.to_string().into_owned(),
                        line: line_info.line_start,
                    });
                }
                _ => {}
            }
        }
    }

    let string_table = pdb.string_table()?;

    let mut files = files
        .into_iter()
        .map(|(filename_string_index, symbols)| {
            let name = filename_string_index.to_raw_string(&string_table)?;
            let name = name.to_string().into_owned();
            Ok(SourceFile { name, symbols })
        })
        .collect::<Result<Vec<_>, pdb::Error>>()?;

    files.sort_by(|x, y| x.name.cmp(&y.name));
    for file in &mut files {
        file.symbols.sort_by_key(|ss| ss.line);
    }
    Ok(files)
}

fn emit(files: &[SourceFile]) {
    for file in files {
        println!("{}", file.name);
        for symbol in &file.symbols {
            println!("{}: {}", symbol.line, symbol.name);
        }
        println!();
    }
}

struct SourceFile {
    name: String,
    symbols: Vec<SourceSymbol>,
}

struct SourceSymbol {
    name: String,
    line: u32,
}
